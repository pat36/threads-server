package threads.share;

public class MimeType {

    public static final String HTML_MIME_TYPE = "text/html";
    public static final String PDF_MIME_TYPE = "application/pdf";
    public static final String OCTET_MIME_TYPE = "application/octet-stream";
    public static final String PLAIN_MIME_TYPE = "text/plain";
    public static final String GEO_MIME_TYPE = "text/geo";
    public static final String LINK_MIME_TYPE = "text/uri-list";
    public static final String TORRENT_MIME_TYPE = "application/x-bittorrent";

}
